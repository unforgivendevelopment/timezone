#!/usr/bin/env bash
#
# scripts/create_release_zip.sh
# -- Part of the 'Timezone' Library for Arduino
#
# Copyright (c) 2017 Gerad Munsch <gmunsch@unforgivendevelopment.com>
#
# DESCRIPTION:
# Creates a ".zip" package of the library, as a release version.
#
